import re
import string
from collections import defaultdict
from copy import deepcopy
from itertools import zip_longest
from typing import Tuple, Union, Any, Dict, List, Optional

import pandas as pd
from loguru import logger
from symspellpy import SymSpell, Verbosity

from pdf_text_correction import constants


def check_if_unknown_char_should_be_space(input_char: str, text: str, spell_check: SymSpell) -> bool:
    """Check if input character should be space, by checking if created words after splitting on input
    character exists in SymSpell vocabulary. Additionally check if splitted word in not a number or punctuation."""

    pattern = re.compile("[\d{}]+$".format(re.escape(string.punctuation)))

    positive_count = 0
    negative_count = 0
    try:
        splitted_word_pairs = get_splitted_word_pairs(input_char, text)
        for splitted_word_pair in splitted_word_pairs:
            for key, value in splitted_word_pair.items():
                # filter out non-alphabetical tokens
                if pattern.match(value):
                    continue
                # check if lowercase and stripped token exists in vocabulary
                if value.lower().strip(',.;') in spell_check.words.keys():
                    positive_count += 1
                else:
                    negative_count += 1
        return positive_count >= negative_count
    except Exception as e:
        return False


def get_splitted_word_pairs(splitting_char: str, text: str) -> List[Dict]:
    """Split input text on input splitting char and return nearest word pairs(one word before and one after)
    for every occurrence of the splitting character."""
    # Find the indexes of the input character in the text
    indexes = [i for i, char in enumerate(text) if char == splitting_char]

    splitted_pairs = []
    for index in indexes:
        before_word = ""
        after_word = ""
        i = index - 1
        j = index + 1

        # Find the nearest word before the index
        while i >= 0 and text[i] != " ":
            before_word = text[i] + before_word
            i -= 1

        # Find the nearest word after the index
        while j < len(text) and text[j] != " ":
            after_word += text[j]
            j += 1

        splitted_pairs.append({'before_word': before_word, 'after_word': after_word})

    return splitted_pairs


def create_font_maps(text_per_font: Dict, spell_check: SymSpell) -> Dict[str, Dict[Any, Any]]:
    """Create font map for every font passed with input tex

    Args:
        text_per_font: dictionary with text grouped by font
        spell_check: spell checking object which will generate replacement candidates for each word from passed text

    Returns:
        Dictionary with font name as key, and it's character mapping as value
    """
    font_maps = {}

    for font, font_text in text_per_font.items():
        text, cids_map = _find_and_replace_cids(font_text)
        font_mapping = _create_map(text, spell_check)

        not_mapped_chars = list(
            set(filter(lambda x: x not in constants.POLISH_ALPHABET and x not in font_mapping.keys(), text)))
        for not_mapped_char in not_mapped_chars:
            if check_if_unknown_char_should_be_space(not_mapped_char, text, spell_check):
                font_mapping[not_mapped_char] = {'right': ' ', 'sum_prob': None, 'sum_support': None}
            else:
                font_mapping[not_mapped_char] = {'right': constants.UNIVERSAL_CHAR_FOR_ALL_NOT_CORRECTED_CHARS,
                                                 'sum_prob': None, 'sum_support': None}

        for cid, unicode_char in cids_map.items():
            if unicode_char in font_mapping.keys():
                font_mapping[cid] = font_mapping.pop(unicode_char)
            else:
                font_mapping[cid] = {'right': constants.UNIVERSAL_CHAR_FOR_ALL_NOT_CORRECTED_CHARS,
                                     'sum_prob': None, 'sum_support': None}

        font_maps[font] = font_mapping

    return font_maps


def update_old_font_maps(text_per_font: Dict, old_font_maps: Dict, spell_check: SymSpell) -> Dict[str, Dict[Any, Any]]:
    """Create font map for every font passed with input tex and compare it with passed old dictionary mapping for
    the same font. Update old font map with values from new one if they are different. Otherwise, change nothing.

    Args:
        text_per_font: dictionary with text grouped by font
        old_font_maps: dictionary with old font map for the same font for comparing
        spell_check: spell checking object which will generate replacement candidates for each word from passed text

    Returns:
        Updated dictionary with font name as key, and it's character mapping as value
    """
    updated_old_font_maps = deepcopy(old_font_maps)
    for font, font_text in text_per_font.items():
        text, cids_map = _find_and_replace_cids(font_text)
        font_mapping = _create_map(text, spell_check)

        not_mapped_chars = list(
            set(filter(lambda x: x not in constants.POLISH_ALPHABET and x not in font_mapping.keys(), text)))
        for not_mapped_char in not_mapped_chars:
            if check_if_unknown_char_should_be_space(not_mapped_char, text, spell_check):
                font_mapping[not_mapped_char] = {'right': ' ', 'sum_prob': None, 'sum_support': None}
            else:
                font_mapping[not_mapped_char] = {'right': constants.UNIVERSAL_CHAR_FOR_ALL_NOT_CORRECTED_CHARS,
                                                 'sum_prob': None, 'sum_support': None}

        for cid, unicode_char in cids_map.items():
            if unicode_char in font_mapping.keys():
                font_mapping[cid] = font_mapping.pop(unicode_char)
            else:
                font_mapping[cid] = {'right': constants.UNIVERSAL_CHAR_FOR_ALL_NOT_CORRECTED_CHARS,
                                     'sum_prob': None, 'sum_support': None}

        for k, v in font_mapping.items():
            if k not in old_font_maps[font]:
                updated_old_font_maps[font][k] = v
            else:
                old_sum_support = old_font_maps[font][k]['sum_support']
                new_sum_support = v['sum_support']

                if new_sum_support is not None:
                    if old_sum_support is None or new_sum_support > old_sum_support:
                        updated_old_font_maps[font][k] = v

    return updated_old_font_maps


def _find_and_replace_cids(text: str) -> Tuple[Union[str, Any], Dict[str, str]]:
    """Find all CID indexes patterns from text

    Args:
        text: text with possible CID indexes as not mapped characters

    Returns:
        Text with replaced CID indexes with specific unicode characters and dictionary with their mappings
    """

    pattern = r"[(]cid:[0-9]*[)]"
    cids_map = {}

    matches = re.finditer(pattern, text, re.MULTILINE)
    for _, match in enumerate(matches, start=1):
        if match.group() not in cids_map:
            number = int(''.join([n for n in match.group() if n.isdigit()]))
            cids_map[match.group()] = chr(constants.MAX_UNICODE_VALUE - number)

    for cid, unicode_char in cids_map.items():
        text = text.replace(cid, unicode_char)

    return text, cids_map


def _create_map(text: str, spell_check: SymSpell) -> Dict[Any, Any]:
    """Create characters mapping for specific font based on passed text. Method use spell checking library which is
    loaded with term frequency for language. Based on generated candidates, method will compare char by char and list
    all differences. Every mapping is associated with probability which is calculated as number of occurrences of
    each map divided by all occurrences.

    Args:
        text: text with possible CID indexes as not mapped characters
        spell_check: spell checking object which will generate replacement candidates for each word from passed text

    Returns:
        Text with replaced CID indexes with specific unicode characters and dictionary with their mappings
    """

    char_maps = []

    for word in text.split():

        if any([char not in constants.POLISH_ALPHABET for char in [*word]]):

            sum_for_word = 0

            for suggestion in spell_check.lookup(word, Verbosity.CLOSEST, max_edit_distance=2, transfer_casing=True,
                                                 include_unknown=True):
                sum_for_word += suggestion.count

            try:

                for suggestion in spell_check.lookup(word, Verbosity.CLOSEST, max_edit_distance=2, transfer_casing=True,
                                                     include_unknown=True):
                    if suggestion.count and len(word) >= 2:
                        word_prob = suggestion.count / sum_for_word
                        try:
                            map_of_diffs = _compare_strings(word, suggestion.term)
                        except Exception as e:
                            logger.error(f'Error during characters comparing - {e}')
                            continue
                        char_map = {map: {'probability': word_prob, 'support': suggestion.count} for map in
                                    map_of_diffs}
                        if char_map:
                            char_maps.append(char_map)
            except ZeroDivisionError:
                continue
        else:
            continue

    grouped_mappings = defaultdict(list)

    for mapping in char_maps:
        for map_pair, map_proba in mapping.items():
            grouped_mappings[map_pair].append(map_proba)

    summed_grouped_mappings = {}
    for key, values in grouped_mappings.items():
        summed_probability = sum(value['probability'] for value in values)
        summed_support = sum(value['support'] for value in values)
        summed_grouped_mappings[key] = {'summed_probability': summed_probability, 'summed_support': summed_support}

    if char_maps:
        df = pd.DataFrame.from_dict(summed_grouped_mappings, orient='index').reset_index()
        df = df.rename(columns={'index': 'map_pair', 'summed_probability': 'sum_prob', 'summed_support': 'sum_support'})
        df[['left', 'right']] = df['map_pair'].str.split(pat='-', n=1, expand=True)
        df = df.loc[df.groupby('left')['sum_prob'].idxmax()]
        df = df.set_index('left')[['right', 'sum_prob', 'sum_support']]
        return df.to_dict(orient='index')

    else:
        return {}


def _compare_strings(a: str, b: str, diffs: Optional[List] = None) -> List[Any]:
    """Compare char by char from two strings and save only differences that out of passed alphabet. Recurisvly find mappings
    for characters with ratio many to many.

    Args:
        a: first string
        b: second string
        diffs: list with already found mappings

    Returns:
        List with characters that differ
    """
    if diffs is None:
        diffs = []

    if len(a) != len(b):
        if len(a) > len(b):
            word1 = a
            word2 = b
        else:
            word1 = b
            word2 = a

        first_index_of_diff = next((i for i, (char1, char2) in enumerate(zip(word1, word2)) if char1 != char2), None)
        if first_index_of_diff is None:
            first_index_of_diff2 = next(
                (i for i, (char1, char2) in enumerate(zip_longest(word1, word2)) if char1 != char2), None)
            if first_index_of_diff2 + 1 > len(word2):
                if word1[first_index_of_diff2] not in constants.POLISH_ALPHABET:
                    diffs.append(word1[first_index_of_diff2] + '-' + '')

                corrected_tmp_word = word1.replace(word1[first_index_of_diff2], '')
                recursive_diffs = _compare_strings(corrected_tmp_word, word2, diffs)
                for char_map in recursive_diffs:
                    if char_map not in diffs:
                        diffs.append(char_map)

        elif first_index_of_diff == 0:
            if word1[first_index_of_diff] not in constants.POLISH_ALPHABET:
                diffs.append(word1[first_index_of_diff] + '-' + '')

            corrected_tmp_word = word1.replace(word1[first_index_of_diff], '')
            recursive_diffs = _compare_strings(corrected_tmp_word, word2, diffs)
            for char_map in recursive_diffs:
                if char_map not in diffs:
                    diffs.append(char_map)
        else:
            common_index_from_two_words = sorted(
                set(word1[first_index_of_diff:]).intersection(word2[first_index_of_diff:]),
                key=lambda x: word1[first_index_of_diff:].index(x))
            if common_index_from_two_words:
                next_index_of_common = word1[first_index_of_diff:].find(common_index_from_two_words[0])

                # check again if corrected word is the one which needs correction
                if any(char not in constants.POLISH_ALPHABET for char in word1):
                    word1, word2 = word2, word1

                if word2[first_index_of_diff] not in constants.POLISH_ALPHABET:
                    diffs.append(word2[first_index_of_diff] + '-' + word1[
                                                                    first_index_of_diff:first_index_of_diff + next_index_of_common])

                    corrected_tmp_word = word2.replace(word2[first_index_of_diff], word1[
                                                                                   first_index_of_diff:first_index_of_diff + next_index_of_common])
                    recursive_diffs = _compare_strings(corrected_tmp_word, word1, diffs)
                    for char_map in recursive_diffs:
                        if char_map not in diffs:
                            diffs.append(char_map)

    else:
        for x, y in zip(a, b):
            if x != y and x not in constants.POLISH_ALPHABET:
                diffs.append(x + '-' + y)

    return diffs


def init_spellcheck(dictionary_path: str):
    """Create SymSpell object with loaded term dictionary from file.

    Args:
        dictionary_path: path to frequency term dictionary for specific language

    Returns:
        SymSpell spell checker
    """
    sym_spell = SymSpell(max_dictionary_edit_distance=2, prefix_length=7)
    sym_spell.load_dictionary(dictionary_path, term_index=0, count_index=1, encoding='utf-8')

    return sym_spell


def correct_ligature(text: str) -> str:
    """Checks whether in text are characters which unicode value is in dictionary with mappings for ligature.
    If yes, then replace all those characters with correct characters from dictionary mapping."""
    for key in constants.LIGATURE_DICT.keys():
        if chr(key) in text:
            text = text.replace(chr(key), constants.LIGATURE_DICT.get(key, ''))
    return text
