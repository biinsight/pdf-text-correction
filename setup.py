from setuptools import setup, find_packages

setup(
    name='pdf-text-correction',
    version='0.0.6',
    description='Library for postprocessing and correcting text',
    license='MIT',
    packages=find_packages(),
    install_requires=['pandas',
                      'symspellpy'],
    author='Piotr Sawicki',
    author_email='psawicki@biinsight.pl',
    keywords=['pdf', 'text', 'fonts', 'mapping'],
    url='https://bitbucket.org/biinsight/pdf-text-correction'
)
